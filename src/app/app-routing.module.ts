import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: () => import('./crud/crud.module').then(m => m.CrudModule) },
  { path: 'slicing', loadChildren: () => import('./slicing/slicing.module').then(m => m.SlicingModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
