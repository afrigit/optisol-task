import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SlicingRoutingModule } from './slicing-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { WrapperComponent } from './wrapper/wrapper.component';
import { SlicingComponent } from './slicing/slicing.component';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    WrapperComponent,
    SlicingComponent
  ],
  imports: [
    CommonModule,
    SlicingRoutingModule
  ]
})
export class SlicingModule { }
