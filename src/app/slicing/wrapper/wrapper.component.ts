import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.scss'],
})
export class WrapperComponent implements OnInit {
  public usersList = [
    {
      name: 'John Doe',
      role: 'Head of Marketing',
      cardImage: '../../../assets/Images/tips-source-1.png',
      userImage: '../../../assets/Images/john-doe.png',
      title: 'Tips for good Pitching Presentation',
      content:
        'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.',
    },
    {
      name: 'Jane Maria',
      role: 'Designer',
      cardImage: '../../../assets/Images/tips-source-2.png',
      userImage: '../../../assets/Images/jane-maria.png',
      title: 'Improve Your Content’s Traffic',
      content:
        'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.',
    },
    {
      name: 'Jack Dorsey',
      role: 'Marketing Specialist',
      cardImage: '../../../assets/Images/tips-source-3.png',
      userImage: '../../../assets/Images/jack-dorsey.png',
      title: 'Reaching Deal with your Clients',
      content:
        'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.',
    }
  ];

  constructor() {}

  ngOnInit(): void {}
}
