import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SlicingComponent } from './slicing/slicing.component';

const routes: Routes = [
  { path: '', component: SlicingComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SlicingRoutingModule { }
