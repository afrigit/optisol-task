import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { User } from 'src/app/Interface/user';
import { UserService } from 'src/app/Services/user.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit, OnDestroy {

  private emailPattern = /[a-zA-Z0-9_.+-,;]+@(?:(?:[a-zA-Z0-9-]+\.,;)?[a-zA-Z]+\.,;)?(gmail)\.com/;
  public userId!: string;
  public userForm!: FormGroup;
  @Output() emitRefreshList = new EventEmitter();
  @Input() set setUserRecord(value: User) {
    if (value && Object.entries(value).length) {
      this.userId = value._id;
      this.userForm.patchValue({
        firstName: value.firstName,
        lastName: value.lastName,
        email: value.email,
        phoneNumber1: value.phoneNumber.slice(0, 2),
        phoneNumber2: value.phoneNumber.slice(2, 12),
        address1: value.address1,
        address2: value.address2,
        city: value.city,
        state: value.state,
        zipCode: value.zipCode,
        country: value.country,
        qualification: value.qualification,
        comments: value.comments
      });
    }
  }
  private createUserSubscription!: Subscription;
  private updateUserSubscription!: Subscription;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.initializeForm();
  }

  private initializeForm(): void {
    this.userForm = new FormGroup({
      firstName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
      lastName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]),
      email: new FormControl('', [Validators.required, Validators.pattern(this.emailPattern)]),
      phoneNumber1: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(2), Validators.maxLength(2)]),
      phoneNumber2: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
      address1: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]),
      address2: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]),
      city: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]),
      state: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]),
      zipCode: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(6), Validators.maxLength(6)]),
      country: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
      qualification: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]),
      comments: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(200)])
    });
  }

  public submit(formDirective: FormGroupDirective): void {
    if(this.userForm.invalid) {
      return;
    }
    const formValue = this.userForm.value;
    formValue['phoneNumber'] = `${formValue['phoneNumber1']}${formValue['phoneNumber2']}`
    if (this.userId) {
      this.userService.updateUser(formValue, this.userId).subscribe(res => {
        formDirective.resetForm();
        this.userForm.reset();
        this.emitRefreshList.emit(true);
        this.userId = '';
      },
        err => {
          console.log('Error', err);
        }
      )
    }
    else {
      this.userService.createUser(formValue).subscribe(res => {
        formDirective.resetForm();
        this.userForm.reset();
        this.emitRefreshList.emit(true);
      },
        err => {
          console.log('Error', err);
        }
      )
    }
  }

  ngOnDestroy(): void {
    this.createUserSubscription?.unsubscribe();
    this.updateUserSubscription?.unsubscribe();
  }

}
