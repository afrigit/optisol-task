import { Component, Input, OnInit, Output, EventEmitter, ViewChild, TemplateRef, OnDestroy } from '@angular/core';
import { User } from 'src/app/Interface/user';
import { UserService } from 'src/app/Services/user.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {

  @Output() listUpdated = new EventEmitter();
  @Output() editUserRecord = new EventEmitter();
  @ViewChild('viewUserInfo') viewUserInfo!: TemplateRef<any>;
  public viewUserRecord!: User;
  private userListSubscription!: Subscription;

  @Input() set refreshList(value: boolean) {
    if (value)
      this.getUsersList();
  }

  public usersList!: User[];

  constructor(private userService: UserService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getUsersList();
  }

  private getUsersList(): void {
    this.userListSubscription = this.userService.getUsersList().subscribe((res: any) => {
      this.usersList = res;
      this.listUpdated.emit(false);
    },
      err => {
        console.log('Error', err);
      }
    )
  }

  public editUser(user: User): void {
    this.editUserRecord.emit(user);
  }

  public deleteUser(userId: string): void {
    this.userService.deleteUser(userId).subscribe((res: any) => {
      this.getUsersList();
    },
      err => {
        console.log('Error', err);
      }
    );
  }

  public viewUser(user: User) {
    this.viewUserRecord = user;
    this.modalService.open(this.viewUserInfo, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
    }, (reason) => {
    });
  }

  ngOnDestroy(): void {
    this.userListSubscription?.unsubscribe();
  }

}
