import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/Interface/user';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  public userRecord!: User;
  public refreshList: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
