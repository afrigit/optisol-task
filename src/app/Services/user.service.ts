import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { User } from '../Interface/user';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = environment.API_URL;
  }

  getUsersList() {
    return this.http.get(`${this.apiUrl}users`)
      .pipe(catchError(this.errorHandler));
  }

  createUser(data: User) {
    return this.http.post(`${this.apiUrl}users`, data).pipe(catchError(this.errorHandler));
  }

  updateUser(data: User, userId: string) {
    return this.http.put(`${this.apiUrl}users/${userId}`, data).pipe(catchError(this.errorHandler));
  }

  deleteUser(userId: string) {
    return this.http.delete(`${this.apiUrl}users/${userId}`).pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.error || "server error.");
  }

}
