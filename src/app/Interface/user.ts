export interface User {
    _id: string;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    address1: string;
    address2: string;
    city: string;
    state: string;
    zipCode: number;
    country: string;
    qualification: string;
    comments: string;
    phoneNumber1?: number;
    phoneNumber2?: number;
}